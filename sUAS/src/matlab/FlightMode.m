classdef FlightMode
    enumeration
        Auto, Loiter, AltHold, RTL, Landing
    end
end

