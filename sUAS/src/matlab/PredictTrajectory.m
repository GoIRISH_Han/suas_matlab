function PredictTrajectory
    global QUAD_ROTOR
    global sUAS_Sim
    
    Pi_dot_prev = QUAD_ROTOR.Pi_dot;
    Att_dot_prev = QUAD_ROTOR.Att_dot;
    Vb_dot_prev = QUAD_ROTOR.Vb_dot;
    Omega_b_dot_prev = QUAD_ROTOR.Omega_b_dot;
    ts = sUAS_Sim.ts;
    State_dot_prev = [Pi_dot_prev; Att_dot_prev; Vb_dot_prev; Omega_b_dot_prev];
    
    quadrotorNonlinearDynamics;
    
    Pi_dot_curr = QUAD_ROTOR.Pi_dot;
    Att_dot_curr = QUAD_ROTOR.Att_dot;
    Vb_dot_curr = QUAD_ROTOR.Vb_dot;
    Omega_b_dot_curr = QUAD_ROTOR.Omega_b_dot;
    State_dot_curr = [Pi_dot_curr; Att_dot_curr; Vb_dot_curr; Omega_b_dot_curr];
    delta = mytrapz(State_dot_prev, State_dot_curr, ts);
    
    QUAD_ROTOR.Pi = QUAD_ROTOR.Pi + delta(1:3);
    QUAD_ROTOR.Att = QUAD_ROTOR.Att + delta(4:6);
    QUAD_ROTOR.Vb = QUAD_ROTOR.Vb + delta(7:9);
    QUAD_ROTOR.Omega_b = QUAD_ROTOR.Omega_b + delta(10:12);
    
end

