function quadrotorNonlinearDynamics
    global QUAD_ROTOR
    
    mass= QUAD_ROTOR.Mass;
    g   = QUAD_ROTOR.g;
    F   = QUAD_ROTOR.F;
    Torq_rho = QUAD_ROTOR.Torq_rho;
    Torq_theta = QUAD_ROTOR.Torq_theta;
    Torq_psi = QUAD_ROTOR.Torq_psi;
    Kdx = QUAD_ROTOR.Kdx;
    Kdy = QUAD_ROTOR.Kdy;
    Kdz = QUAD_ROTOR.Kdz;
    
    Jxx = QUAD_ROTOR.Jxx;
    Jyy = QUAD_ROTOR.Jyy;
    Jzz = QUAD_ROTOR.Jzz;
    
    rho     = QUAD_ROTOR.Att(1);
    theta   = QUAD_ROTOR.Att(2);
    psi     = QUAD_ROTOR.Att(3);
    
    u       = QUAD_ROTOR.Vb(1);
    v       = QUAD_ROTOR.Vb(2);
    w       = QUAD_ROTOR.Vb(3);
    
    p       = QUAD_ROTOR.Omega_b(1);
    q       = QUAD_ROTOR.Omega_b(2);
    r       = QUAD_ROTOR.Omega_b(3);
    
    
    Pi_dot      =   [cos(theta)*cos(psi), sin(rho)*sin(theta)*cos(psi)-cos(rho)*sin(psi), cos(rho)*sin(theta)*cos(psi)+sin(rho)*sin(psi);
        cos(theta)*sin(psi), sin(rho)*sin(theta)*sin(psi)+cos(rho)*cos(psi), cos(rho)*sin(theta)*sin(psi)-sin(rho)*cos(psi);
        -sin(theta),         sin(rho)*cos(theta),                            cos(rho)*cos(theta)] * [u; v; w];
    QUAD_ROTOR.Pi_dot = Pi_dot;
    
    % pn_dot      =   Pi_dot(1);
    % pe_dot      =   Pi_dot(2);
    % h_dot       =   Pi_dot(3);
    
    Vb_dot      =   [r*v - q*w; p*w - r*u; q*u - p*v] + [-g*sin(theta); g*cos(theta)*sin(rho); g*cos(theta)*cos(rho)-F/mass]-[Kdx*u; Kdy*v; Kdz*w]/mass;
    QUAD_ROTOR.Vb_dot = Vb_dot;
    % u_dot       =   Vb_dot(1);
    % v_dot       =   Vb_dot(2);
    % w_dot       =   Vb_dot(3);
    
    Att_dot     =   [1,  sin(rho)*tan(theta), cos(rho)*tan(theta);
        0,  cos(rho),            -sin(rho);
        0,  sin(rho)/cos(theta), cos(rho)/cos(theta)] * [p; q; r];
    QUAD_ROTOR.Att_dot = Att_dot;
    % rho_dot     =   Att_dot(1);
    % theta_dot   =   Att_dot(2);
    % psi_dot     =   Att_dot(3);
    
    Omega_b_dot =   [(Jyy-Jzz)/Jxx * q*r; (Jzz-Jxx)/Jyy * p*r; (Jxx-Jyy)/Jzz*p*q] +  [Torq_rho/Jxx; Torq_theta/Jyy; Torq_psi/Jzz];
    QUAD_ROTOR.Omega_b_dot = Omega_b_dot;
    % p_dot       =   Omega_b_dot(1);
    % q_dot       =   Omega_b_dot(2);
    % r_dot       =   Omega_b_dot(3);
end

