function simTraj
    global sUAS_Sim
    global QUAD_ROTOR
    global OuterLoopControl
    global InnerLoopControl
    global IOLinearizationControl
    persistent k
    
    if (sUAS_Sim.time == 0)
        k = 1; 
    end
    
    for i = 1 : length(sUAS_Sim.tspan)
        OuterLoopControl.refInputs.pn_d = sUAS_Sim.refTraj.pn_d;
        OuterLoopControl.refInputs.pe_d = sUAS_Sim.refTraj.pe_d;
        OuterLoopControl.refInputs.pn_vel_d = sUAS_Sim.refTraj.pn_vel_d;
        OuterLoopControl.refInputs.pe_vel_d = sUAS_Sim.refTraj.pe_vel_d;
        OuterLoopControlUpdate;
        
        InnerLoopControl.refInputs.h_d = sUAS_Sim.refTraj.h_d;
        InnerLoopControl.refInputs.rho_d = OuterLoopControl.ctrInputs.rho_d;
        InnerLoopControl.refInputs.theta_d = OuterLoopControl.ctrInputs.theta_d;
        InnerLoopControl.refInputs.psi_d = sUAS_Sim.refTraj.psi_d;
        InnerLoopControl.refInputs.h_vel_d = sUAS_Sim.refTraj.h_vel_d;
        InnerLoopControl.refInputs.rho_vel_d = 0;
        InnerLoopControl.refInputs.theta_vel_d = 0;
        InnerLoopControl.refInputs.psi_vel_d = 0;
        InnerLoopControlUpdate;
        
        IOLinearizationControl.refInputs.v1 = InnerLoopControl.ctrInputs.v1;
        IOLinearizationControl.refInputs.v2 = InnerLoopControl.ctrInputs.v2;
        IOLinearizationControl.refInputs.v3 = InnerLoopControl.ctrInputs.v3;
        IOLinearizationControl.refInputs.v4 = InnerLoopControl.ctrInputs.v4;
        IOLinearizationUpdate;
        
        QUAD_ROTOR.F = IOLinearizationControl.ctrInputs.F;
        QUAD_ROTOR.Torq_rho = IOLinearizationControl.ctrInputs.Torq_rho;
        QUAD_ROTOR.Torq_theta = IOLinearizationControl.ctrInputs.Torq_theta;
        QUAD_ROTOR.Torq_psi = IOLinearizationControl.ctrInputs.Torq_psi;
        PredictTrajectory;
        sUAS_Sim.predTraj.Pi(:,i + (k-1)*length(sUAS_Sim.tspan)) = QUAD_ROTOR.Pi;
        sUAS_Sim.predTraj.Att(:,i + (k-1)*length(sUAS_Sim.tspan)) = QUAD_ROTOR.Att;
        sUAS_Sim.predTraj.Pi_dot(:,i + (k-1)*length(sUAS_Sim.tspan)) = QUAD_ROTOR.Pi_dot;
        sUAS_Sim.time = sUAS_Sim.time + sUAS_Sim.ts;
        if(~sUAS_Sim.enableFltPlan)
            fprintf('%2s %.2f \t %s %.2f \t %s %.2f\n ','Pn_sim := ', QUAD_ROTOR.Pi(1), 'Pe_sim :=', QUAD_ROTOR.Pi(2), 'H_sim :=', QUAD_ROTOR.Pi(3));
            plot3(QUAD_ROTOR.Pi(1),QUAD_ROTOR.Pi(2),QUAD_ROTOR.Pi(3),'b*');
            hold on;
        end
    end
    k = k+1; 
end

