function InnerLoopControlUpdate
    global QUAD_ROTOR
    global InnerLoopControl
    
    K1_h = 1;
    K2_h = 1;
    h_dot_max = InnerLoopControl.refInputs.h_vel_Max;
    h_dot_ref = InnerLoopControl.refInputs.h_vel_d;
    h_ref = InnerLoopControl.refInputs.h_d;
    h_dot = QUAD_ROTOR.Pi_dot(3);
    h = QUAD_ROTOR.Pi(3);
    h_accel_d = ConstrainedMinTimeOptimalControl(K1_h, K2_h, h_dot_max, h_dot_ref, h_ref, h_dot, h);
    
    K1_rho = 1;
    K2_rho = 1;
    rho_dot_max = InnerLoopControl.refInputs.rho_vel_Max;
    rho_dot_ref = InnerLoopControl.refInputs.rho_vel_d;
    rho_ref = InnerLoopControl.refInputs.rho_d;
    rho_dot = QUAD_ROTOR.Att_dot(1);
    rho = QUAD_ROTOR.Att(1);
    rho_accel_d = ConstrainedMinTimeOptimalControl(K1_rho, K2_rho, rho_dot_max, rho_dot_ref, rho_ref, rho_dot, rho);
    
    K1_theta = 1;
    K2_theta = 1;
    theta_dot_max = InnerLoopControl.refInputs.theta_vel_Max;
    theta_dot_ref = InnerLoopControl.refInputs.theta_vel_d;
    theta_ref = InnerLoopControl.refInputs.theta_d;
    theta_dot = QUAD_ROTOR.Att_dot(2);
    theta = QUAD_ROTOR.Att(2);
    theta_accel_d = ConstrainedMinTimeOptimalControl(K1_theta, K2_theta, theta_dot_max, theta_dot_ref, theta_ref, theta_dot, theta);
    
    K1_psi = 1;
    K2_psi = 1;
    psi_dot_max = InnerLoopControl.refInputs.psi_vel_Max;
    psi_dot_ref = InnerLoopControl.refInputs.psi_vel_d;
    psi_ref = InnerLoopControl.refInputs.psi_d;
    psi_dot = QUAD_ROTOR.Att_dot(3);
    psi = QUAD_ROTOR.Att(3);
    psi_accel_d = ConstrainedMinTimeOptimalControl(K1_psi, K2_psi, psi_dot_max, psi_dot_ref, psi_ref, psi_dot, psi);
    
    InnerLoopControl.ctrInputs.v1 = h_accel_d;
    InnerLoopControl.ctrInputs.v2 = rho_accel_d;
    InnerLoopControl.ctrInputs.v3 = theta_accel_d;
    InnerLoopControl.ctrInputs.v4 = psi_accel_d;
end

