function [X,Y,Z] = gen3DTrajPattern(radius, height)
th = 0:pi/50:2*pi;
X = radius * cos(th) + radius;
Y = radius * sin(th) + radius;
Z = th * height; 
end

