% Small UAS Trajectory Simulator 
% Author: Han Yu
clear all;
close all; 
clc;

global constant
constant.r2d = 180/pi;                              % conversion factor from radian to degree 
constant.d2r = pi/180;                              % conversion factor from degree to radian

global sUAS_Sim;
sUAS_Sim.enableFltPlan = 1;                         % switch to turn on autonomous flying with an input flight plan
sUAS_Sim.span = 5;                                  % simulation duration for each simulation window  
sUAS_Sim.ts = 1/1000;                               % sampling time 
sUAS_Sim.tspan = 0 : sUAS_Sim.ts : sUAS_Sim.span;   % discretized simulation time for each simulation window
sUAS_Sim.time = 0;                                  % current simulation time
if (sUAS_Sim.enableFltPlan == 0)
    sUAS_Sim.refTraj.h_d = 1;                      % reference trajectory if no input flight plan: altitude[m]
    sUAS_Sim.refTraj.pn_d = 1;                     % reference trajectory if no input flight plan: x [m]
    sUAS_Sim.refTraj.pe_d = 1;                     % reference trajectory if no input flight plan: y [m]
    sUAS_Sim.refTraj.psi_d = 1;                     % reference trajectory default heading/yaw [rad]
    sUAS_Sim.fltTime = 0 : sUAS_Sim.ts : sUAS_Sim.tspan; 
else
    sUAS_Sim.refTraj.radius = 10; 
    sUAS_Sim.refTraj.height = 30;
    [X, Y, Z] = gen3DTrajPattern(sUAS_Sim.refTraj.radius, sUAS_Sim.refTraj.height);     % generate waypoints based on trajectory pattern
    sUAS_Sim.fltPlan.pn = X; 
    sUAS_Sim.fltPlan.pe = Y; 
    sUAS_Sim.fltPlan.h = Z; 
    sUAS_Sim.refTraj.psi_d = 1;                     % reference trajectory default heading/yaw [rad]
    sUAS_Sim.fltTime = 0 : sUAS_Sim.ts: (sUAS_Sim.span * length(sUAS_Sim.fltPlan.pn));
end

sUAS_Sim.refTraj.h_vel_d = 0;                       % reference trajectory speed in z, set to 0 by default [m/s] 
sUAS_Sim.refTraj.pn_vel_d = 0;                      % reference trajectory speed in x, set to 0 by default [m/s]
sUAS_Sim.refTraj.pe_vel_d = 0;                      % reference trajectory speed in y, set to 0 by default [m/s]


global QUAD_ROTOR; 
QUAD_ROTOR.Att_init = [0;0;0];                      % initial attitude (Euler angles) Roll,Pitch,Yaw [rad]
QUAD_ROTOR.Omega_b_init = [0;0;0];                  % initial angular rates in Body frame [rad/s]
QUAD_ROTOR.Vb_init=[0;0;0];                         % initial velocity in in Body frame [m/s]
if(sUAS_Sim.enableFltPlan == 0)
    QUAD_ROTOR.Pi_init = [ 0; 0; 0];                % initial position in Inertial frame, default is at the origin [m]
else
    QUAD_ROTOR.Pi_init = [sUAS_Sim.fltPlan.pn(1); sUAS_Sim.fltPlan.pe(1); 0]; % initial position in Inertial frame referring to the first waypoint in flight plan [m]
end

QUAD_ROTOR.Att_dot_init = [0;0;0];                  % initial angluar rate (Euler angles) [rad/s]
QUAD_ROTOR.Omega_b_dot_init = [0;0;0];              % initial angluar rate (body frame) [rad/s]
QUAD_ROTOR.Vb_dot_init = [0;0;0];                   % initial translation velocity (body frame) [m/s]
QUAD_ROTOR.Pi_dot_init = [0;0;0];                   % initial translation velocity (inertial frame) [m/s]
QUAD_ROTOR.F_init = 0;                              % initial thrust [N]
QUAD_ROTOR.Torq_rho_init = 0;                       % initial moment [N*m]
QUAD_ROTOR.Torq_theta_init = 0;                     % initial moment [N*m]
QUAD_ROTOR.Torq_psi_init = 0;                       % initial moment [N*m]

QUAD_ROTOR.g = 9.80;                                % acceleration of gravity [m/s^2]
QUAD_ROTOR.Mass = 1.5;                              % complete airframe mass[kg] 
QUAD_ROTOR.Jxx = 0.05;                              % moment of inertia for multirotor w.r.t center of gravity [kg*m^2]
QUAD_ROTOR.Jyy = 0.05;                              % moment of inertia for multirotor w.r.t center of gravity [kg*m^2]
QUAD_ROTOR.Jzz = 0.3;                               % moment of inertia for multirotor w.r.t center of gravity [kg*m^2]   
QUAD_ROTOR.Kdx = 0.1648;                            % translational drag force(or friction) coeffecient [kg/s]
QUAD_ROTOR.Kdy = 0.31892;                           % translational drag force(or friction) coeffecient [kg/s]
QUAD_ROTOR.Kdz = 1.1E-6;                            % translational drag force(or friction) coeffecient [kg/s]

QUAD_ROTOR.Att = QUAD_ROTOR.Att_init;               % Euler angles rho, theta, psi [rad]
QUAD_ROTOR.Omega_b = QUAD_ROTOR.Omega_b_init;       % angular rate in body frame, p, q, r [rad/s] 
QUAD_ROTOR.Vb = QUAD_ROTOR.Vb_init;                 % velocity in body frame, u, v, w [m/s]
QUAD_ROTOR.Pi = QUAD_ROTOR.Pi_init;                 % position in inertial frame, pn, pe, h [m]
QUAD_ROTOR.Att_dot = QUAD_ROTOR.Att_dot_init;       % angluar rate (Euler angles) [rad/s]
QUAD_ROTOR.Omega_b_dot = QUAD_ROTOR.Omega_b_dot_init;   % angluar rate (body frame) [rad/s]
QUAD_ROTOR.Vb_dot = QUAD_ROTOR.Vb_dot_init;             % translation velocity (body frame) [m/s]
QUAD_ROTOR.Pi_dot = QUAD_ROTOR.Pi_dot_init;             % translation velocity (inertial frame) [m/s]
QUAD_ROTOR.F = QUAD_ROTOR.F_init;                       % thrust [N]
QUAD_ROTOR.Torq_rho = QUAD_ROTOR.Torq_rho_init;         % moment [N*m]
QUAD_ROTOR.Torq_theta = QUAD_ROTOR.Torq_theta_init;     % moment [N*m]                     
QUAD_ROTOR.Torq_psi = QUAD_ROTOR.Torq_psi_init;         % moment [N*m]  
QUAD_ROTOR.acPerformance.h_vel_Max = 10;                % maximum velocity in z [m/s] 
QUAD_ROTOR.acPerformance.pn_vel_Max = 10;               % maximum velocity in x [m/s] 
QUAD_ROTOR.acPerformance.pe_vel_Max = 10;               % maximum velocity in z [m/s] 
QUAD_ROTOR.acPerformance.rho_vel_Max = 5;               % maximum roll rate [rad/s]
QUAD_ROTOR.acPerformance.theta_vel_Max = 5;             % maximum pitch rate [rad/s] 
QUAD_ROTOR.acPerformance.psi_vel_Max = 5;               % maximum yaw rate [rad/s]



global InnerLoopControl
InnerLoopControl.refInputs.h_d = 0; 
InnerLoopControl.refInputs.rho_d = 0; 
InnerLoopControl.refInputs.theta_d = 0; 
InnerLoopControl.refInputs.psi_d = 0;
InnerLoopControl.refInputs.h_vel_d = 0; 
InnerLoopControl.refInputs.rho_vel_d = 0; 
InnerLoopControl.refInputs.theta_vel_d = 0; 
InnerLoopControl.refInputs.psi_vel_d = 0; 

InnerLoopControl.refInputs.h_vel_Max = QUAD_ROTOR.acPerformance.h_vel_Max;           % maximum velocity in z [m/s] 
InnerLoopControl.refInputs.rho_vel_Max = QUAD_ROTOR.acPerformance.rho_vel_Max;       % maximum roll rate [rad/s]
InnerLoopControl.refInputs.theta_vel_Max = QUAD_ROTOR.acPerformance.theta_vel_Max;   % maximum pitch rate [rad/s] 
InnerLoopControl.refInputs.psi_vel_Max = QUAD_ROTOR.acPerformance.psi_vel_Max;       % maximum yaw rate [rad/s]

InnerLoopControl.ctrInputs.v1 = 0; 
InnerLoopControl.ctrInputs.v2 = 0; 
InnerLoopControl.ctrInputs.v3 = 0; 
InnerLoopControl.ctrInputs.v4 = 0;

global IOLinearizationControl 
IOLinearizationControl.refInputs.v1 = 0; 
IOLinearizationControl.refInputs.v2 = 0;
IOLinearizationControl.refInputs.v3 = 0;
IOLinearizationControl.refInputs.v4 = 0;
IOLinearizationControl.ctrInputs.F = 0; 
IOLinearizationControl.ctrInputs.Torq_rho = 0;
IOLinearizationControl.ctrInputs.Torq_theta = 0;
IOLinearizationControl.ctrInputs.Torq_psi = 0;


global OuterLoopControl
OuterLoopControl.refInputs.pn_d = 0;
OuterLoopControl.refInputs.pe_d = 0;
OuterLoopControl.refInputs.pn_vel_d = 0;
OuterLoopControl.refInputs.pe_vel_d = 0;
OuterLoopControl.refInputs.pn_vel_Max = QUAD_ROTOR.acPerformance.pn_vel_Max;    % maximum velocity in x [m/s] 
OuterLoopControl.refInputs.pe_vel_Max = QUAD_ROTOR.acPerformance.pe_vel_Max;    % maximum velocity in y [m/s] 
OuterLoopControl.lateralPositionControl.Kp = 0.4;                               % default PID control gain: proportional gain
OuterLoopControl.lateralPositionControl.Ki = 0.001;                             % default PID control gain: integration gain
OuterLoopControl.lateralPositionControl.Kd = 0.8;                               % default PID control gain: derivative gain
OuterLoopControl.ctrInputs.theta_d = 0;
OuterLoopControl.ctrInputs.rho_d = 0;

sUAS_Sim.predTraj.Pi = zeros(length(QUAD_ROTOR.Pi), length(sUAS_Sim.fltTime)); 
sUAS_Sim.predTraj.Att = zeros(length(QUAD_ROTOR.Att), length(sUAS_Sim.fltTime)); 
sUAS_Sim.predTraj.Pi_dot = zeros(length(QUAD_ROTOR.Pi_dot), length(sUAS_Sim.fltTime));
if(sUAS_Sim.enableFltPlan)
    for i = 1 : length(sUAS_Sim.fltPlan.pn)
        sUAS_Sim.refTraj.pn_d = sUAS_Sim.fltPlan.pn(i);
        sUAS_Sim.refTraj.pe_d = sUAS_Sim.fltPlan.pe(i);
        sUAS_Sim.refTraj.h_d = sUAS_Sim.fltPlan.h(i);
        fprintf('% 2s %.2f \t %2s %.2f \t %s %.2f \t %s %.2f\n ', 'time := ', sUAS_Sim.time, 'Pn_ref := ', sUAS_Sim.refTraj.pn_d, 'Pe_ref :=', sUAS_Sim.refTraj.pe_d, 'H_ref :=', sUAS_Sim.refTraj.h_d);
        figure(1); 
        plot3(sUAS_Sim.refTraj.pn_d ,sUAS_Sim.refTraj.pe_d,sUAS_Sim.refTraj.h_d ,'ro-'); 
        simTraj;
        fprintf('% 2s %.2f \t %2s %.2f \t %s %.2f \t %s %.2f\n ','time := ', sUAS_Sim.time, 'Pn_sim := ', QUAD_ROTOR.Pi(1), 'Pe_sim :=', QUAD_ROTOR.Pi(2), 'H_sim :=', QUAD_ROTOR.Pi(3));
        plot3(QUAD_ROTOR.Pi(1),QUAD_ROTOR.Pi(2),QUAD_ROTOR.Pi(3),'b*');
        hold on;
    end
else
    fprintf('% 2s %.2f \t %2s %.2f \t %s %.2f \t %s %.2f\n ','time := ', sUAS_Sim.time,'Pn_ref := ', sUAS_Sim.refTraj.pn_d, 'Pe_ref :=', sUAS_Sim.refTraj.pe_d, 'H_ref :=', sUAS_Sim.refTraj.h_d);
    plot3(sUAS_Sim.refTraj.pn_d,sUAS_Sim.refTraj.pe_d,sUAS_Sim.refTraj.h_d,'ro-');
    hold on;
    simTraj;
end
xlabel('X (m)');
ylabel('Y (m)');
zlabel('Z (m)');
legend('Reference Trajectory', 'Predicted Trajectory');










