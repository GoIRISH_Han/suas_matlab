function buildFltPlan(Home,Waypoints)
global FltPlan 
global sUAS_Sim

if(~sUAS_Sim.enableFltPlan)
    FltPlan.Home.id = 'Home';
    FltPlan.Home.X = Home.X; 
    FltPlan.Home.Y = Home.Y; 
    FltPlan.Home.Z = Home.Z; 
    return
end

for i = 1 : length(Waypoints)
    FltPlan.Waypoints(i).id = "WPT_"+int2str(i); 
    FltPlan.Waypoints(i).X = Waypoints(i).X; 
    FltPlan.Waypoints(i).Y = Waypoints(i).Y;
    FltPlan.Waypoints(i).Z = Waypoints(i).Z; 
end

end

