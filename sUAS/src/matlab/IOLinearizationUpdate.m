function IOLinearizationUpdate

global QUAD_ROTOR
global IOLinearizationControl

Jxx  = QUAD_ROTOR.Jxx; 
Jyy  = QUAD_ROTOR.Jyy; 
Jzz  = QUAD_ROTOR.Jzz;
mass = QUAD_ROTOR.Mass;
g    = QUAD_ROTOR.g; 
rho_dot = QUAD_ROTOR.Att_dot(1);
theta_dot = QUAD_ROTOR.Att_dot(2); 
psi_dot = QUAD_ROTOR.Att_dot(3); 
rho = QUAD_ROTOR.Att(1);
theta = QUAD_ROTOR.Att(2); 

B = [g; theta_dot*psi_dot*(Jyy-Jxx)/Jxx; rho_dot*psi_dot*(Jzz-Jxx)/Jyy; rho_dot*theta_dot*(Jxx-Jyy)/Jzz]; 

v = [IOLinearizationControl.refInputs.v1; IOLinearizationControl.refInputs.v2; 
     IOLinearizationControl.refInputs.v3; IOLinearizationControl.refInputs.v4];


% Delta = [-cos(theta)*cos(rho)/mass 0 0 0;
%      0 1/Jxx 0 0; 
%      0 0 1/Jyy 0; 
%      0 0 0 1/Jzz];

Delta_inv = [-mass/(cos(theta)*cos(rho)) 0 0 0; 
             0 Jxx 0 0; 
             0 0 Jyy 0; 
             0 0 0 Jzz]; 
         
Alpha = -Delta_inv*B; 

Belta = Delta_inv; 

U = Alpha + Belta * v;  

IOLinearizationControl.ctrInputs.F = U(1); 
IOLinearizationControl.ctrInputs.Torq_rho = U(2);
IOLinearizationControl.ctrInputs.Torq_theta = U(3);
IOLinearizationControl.ctrInputs.Torq_psi = U(4); 

end

