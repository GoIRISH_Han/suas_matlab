function[x_accel_d, y_accel_d] = LateralPositionControl
    % current implemetation is just to fly waypoints, not for motion tracking
    global sUAS_Sim
    global QUAD_ROTOR
    global OuterLoopControl
    persistent posErrPrev
    persistent integrator
    persistent differentiator
    
    ref.Pn = sUAS_Sim.refTraj.pn_d;
    ref.Pe = sUAS_Sim.refTraj.pe_d;
    ref.Pn_vel = sUAS_Sim.refTraj.pn_vel_d;
    ref.Pe_vel = sUAS_Sim.refTraj.pe_vel_d;
    ref.Pn_accl = 0; % set to 0 by default
    ref.Pe_accl = 0; % set to 0 by default
    tau = sUAS_Sim.ts/2;
    
    if (sUAS_Sim.time == 0)
        posErrPrev = [0;0];
        integrator = [0;0];
        differentiator = [0;0];
    end
    Pn = QUAD_ROTOR.Pi(1);
    Pe = QUAD_ROTOR.Pi(2);
    posErrCurr = [ref.Pn;ref.Pe] - [Pn;Pe];
    integrator = mytrapz(posErrPrev, posErrCurr, sUAS_Sim.ts) + integrator;
    differentiator = (2*tau - sUAS_Sim.ts)/(2*tau + sUAS_Sim.ts) * differentiator+ 2/(2*tau +sUAS_Sim.ts)*(posErrCurr-posErrPrev);
    %differentiator = -[QUAD_ROTOR.Pi_dot(1); QUAD_ROTOR.Pi_dot(2)];
    U = OuterLoopControl.lateralPositionControl.Kp * posErrCurr + OuterLoopControl.lateralPositionControl.Ki * integrator + OuterLoopControl.lateralPositionControl.Kd * differentiator;
    x_accel_d = sat(U(1),1);
    y_accel_d = sat(U(2),1);
    posErrPrev = posErrCurr;
    
end

