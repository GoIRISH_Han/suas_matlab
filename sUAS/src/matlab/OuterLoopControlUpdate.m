function OuterLoopControlUpdate
    global QUAD_ROTOR
    global OuterLoopControl
    
    mass = QUAD_ROTOR.Mass;
    F = QUAD_ROTOR.F;
    
    % K1_pn = 1;
    % K2_pn = 1;
    % pn_dot_max = OuterLoopControl.refInputs.pn_vel_Max;
    % pn_dot_ref = OuterLoopControl.refInputs.pn_vel_d;
    % pn_ref = OuterLoopControl.refInputs.pn_d;
    % pn_dot = QUAD_ROTOR.Pi_dot(1);
    % pn = QUAD_ROTOR.Pi(1);
    % x_accel_d = ConstrainedMinTimeOptimalControl(K1_pn, K2_pn, pn_dot_max, pn_dot_ref, pn_ref, pn_dot, pn);
    %
    % K1_pe = 1;
    % K2_pe = 1;
    % pe_dot_max = OuterLoopControl.refInputs.pe_vel_Max;
    % pe_dot_ref = OuterLoopControl.refInputs.pe_vel_d;
    % pe_ref = OuterLoopControl.refInputs.pe_d;
    % pe_dot = QUAD_ROTOR.Pi_dot(2);
    % pe = QUAD_ROTOR.Pi(2);
    % y_accel_d = ConstrainedMinTimeOptimalControl(K1_pe, K2_pe, pe_dot_max, pe_dot_ref, pe_ref, pe_dot, pe);
    
    [x_accel_d, y_accel_d] = LateralPositionControl;
    
    psi = QUAD_ROTOR.Att(3);
    
    U = -mass/F*[sin(psi), -cos(psi); cos(psi), sin(psi)]*[x_accel_d;y_accel_d];
    OuterLoopControl.ctrInputs.rho_d = U(1);
    OuterLoopControl.ctrInputs.theta_d = U(2);
    
end

