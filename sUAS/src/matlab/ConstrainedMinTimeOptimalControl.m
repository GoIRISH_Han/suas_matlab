function U = ConstrainedMinTimeOptimalControl(K1, K2, X1_max, X1_ref, X2_ref, x1, x2)
    A = (x1 > -X1_max && x1 < X1_max);
    B = (x1 >= -X1_max && x1 <= X1_max); 
    C = K2/(2*K1)*sign(X1_ref-x1)*(x1^2-X1_ref^2)+X2_ref;
    D = K2/(2*K1)*(x1^2-X1_ref^2)+X2_ref;

    if ((A && x2<C) || (B && (x1<X1_ref) && x2 ==D))
        U = 1;
    elseif ((x1==-X1_max && (x2~=D)) || (x1==X1_ref && x2==X2_ref) || (x1 == X1_max && (x2~=-D)))
        U = 0; 
    elseif ((A && x2>C) || (B && (x1>X1_ref) && (x2 == -D)))
        U = -1; 
    else 
        U = 0;
    end
end
